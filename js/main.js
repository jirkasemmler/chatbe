/**
 * Created by xkohou03 on 26.09.16.
 */

$(document).on("click", ".btn-details", function(e) {
    e.preventDefault();
    $(this).toggleClass("active");
});


$( document ).ready(function() {

    var options = {
        useEasing: true,
        useGrouping: true,
        separator: '',
        decimal: '.',
        prefix: '',
        suffix: ''
    };

    var counter_accounts = $("#counter-accounts");
    var counter_operators = $("#counter-operators");
    var counter_domains = $("#counter-domains");
    new CountUp(counter_accounts.attr("id"), 0, counter_accounts.data("number"), 0, 2.5, options).start();
    new CountUp(counter_operators.attr("id"), 0, counter_operators.data("number"), 0, 2.5, options).start();
    new CountUp(counter_domains.attr("id"), 0, counter_domains.data("number"), 0, 2.5, options).start();


});